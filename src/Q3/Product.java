package Q3;

public class Product implements Taxable {
	String name;
	double value;
	public Product(String n,double v){
		name = n;
		value = v;
		
	}
	@Override
	public double getTax() {
		double ans = (value*7)/100;
		return ans;
	}

}
