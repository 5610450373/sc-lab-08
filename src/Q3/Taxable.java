package Q3;

public interface Taxable {
	double getTax();
}
