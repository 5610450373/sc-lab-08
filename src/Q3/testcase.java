package Q3;

import java.util.ArrayList;

public class testcase {
	public static void main(String[] args){
		ArrayList<Taxable> lis1 = new ArrayList<Taxable>();
		ArrayList<Taxable> lis2 = new ArrayList<Taxable>();
		ArrayList<Taxable> lis3 = new ArrayList<Taxable>();
		ArrayList<Taxable> lis4 = new ArrayList<Taxable>();
		
		Person p = new Person("P1",500000);
		Person p2 = new Person("P2",300000);
		Person p3 = new Person("P3",100000);
		lis1.add(p);
		lis1.add(p2);
		lis1.add(p3);
		Company c = new Company("C1", 1000000,800000);
		Company c2 = new Company("C2", 1000000,500000);
		Company c3 = new Company("C3", 1000000,100000);
		lis2.add(c);
		lis2.add(c2);
		lis2.add(c3);
		Product pd = new Product("pd1", 100);
		Product pd2 = new Product("pd2", 200);
		Product pd3 = new Product("pd3", 300);
		lis3.add(pd);
		lis3.add(pd2);
		lis3.add(pd3);
		//lis4
		lis4.add(p);
		lis4.add(p2);
		lis4.add(p3);
		lis4.add(c);
		lis4.add(c2);
		lis4.add(c3);
		lis4.add(pd);
		lis4.add(pd2);
		lis4.add(pd3);
		
		System.out.println(TaxCalculator.sum(lis1));
		System.out.println(TaxCalculator.sum(lis2));
		System.out.println(TaxCalculator.sum(lis3));
		System.out.println(TaxCalculator.sum(lis4));
	}
}
