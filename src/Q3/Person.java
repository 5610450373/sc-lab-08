package Q3;

public class Person implements Taxable {
	String name;
	double value;
	public Person(String n,double v){
		name = n;
		value = v;
	}

	@Override
	public double getTax() {
		double sum=0;
		if(value>300000){
			double tax1 = (300000*5)/100;
			double tax2 = ((value-300000)*10)/100;
			sum = tax1+tax2;
			}
		else{
			double tax = (300000*5)/100;
			sum=tax;
		}
		return sum;
	}

}
