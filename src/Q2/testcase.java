package Q2;

public class testcase {
	public static void main(String[] args){
		Person p = new Person("A", 160);
		Person p2 = new Person("B", 170);
		Measurable answer = Data.min(p2,p);
		System.out.println("Value of min is "+answer.getMeasure()+"   (Person)");
		
		BankAccount b1 = new BankAccount(1500);
		BankAccount b2 = new BankAccount(1000);
		answer = Data.min(b1, b2);
		System.out.println("Value of min is "+answer.getMeasure()+"   (BankAccount)");
		
		Country c1 = new Country("C1", 500);
		Country c2 = new Country("C2",340);
		answer = Data.min(c1,c2);
		System.out.println("Value of min is "+answer.getMeasure()+"   (Country)");
		
	}
}	
